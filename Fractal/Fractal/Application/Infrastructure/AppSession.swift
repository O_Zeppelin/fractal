//
//  AppSession.swift
//  ArmazemPB-iOS
//
//  Created by Anderson Silva on 12/07/17.
//  Copyright © 2017 Wyllamys Cavalcante. All rights reserved.
//

import Foundation

struct LikesBeers {
    var beers:Beers
    var like:Bool
}

class AppSession {
    
    static let sharedInstance : AppSession = {
        let instance = AppSession()
        return instance
    }()
    
    var likes:[LikesBeers] = [LikesBeers]()
    
}






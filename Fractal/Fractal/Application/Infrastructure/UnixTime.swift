//
//  UnixTime.swift
//  ArmazemPB-iOS
//
//  Created by Anderson Silva on 11/07/17.
//  Copyright © 2017 Wyllamys Cavalcante. All rights reserved.
//

import Foundation

typealias UnixTime = Int

extension UnixTime {
    private func formatType(form: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "pt_BR")
        dateFormatter.dateFormat = form
        return dateFormatter
    }
    
    var dateFull: Date {
        return Date(timeIntervalSince1970: Double(self))
    }
    
    var toHour: String {
        return formatType(form: "HH:mm").string(from: dateFull)
    }
    
    var toDay: String {
        return formatType(form: "dd/MM/yyyy").string(from: dateFull)
    }
}

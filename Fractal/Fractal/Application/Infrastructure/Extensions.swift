//
//  Extensions.swift
//  ArmazemPB-iOS
//
//  Created by Anderson Silva on 11/07/17.
//  Copyright © 2017 Wyllamys Cavalcante. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    public func dch_checkDeallocation(afterDelay delay: TimeInterval = 2.0) {
        let rootParentViewController = dch_rootParentViewController
        
        // We don’t check `isBeingDismissed` simply on this view controller because it’s common
        // to wrap a view controller in another view controller (e.g. in UINavigationController)
        // and present the wrapping view controller instead.
        if isMovingFromParentViewController || rootParentViewController.isBeingDismissed {
            let type = type(of: self)
            let disappearanceSource: String = isMovingFromParentViewController ? "removed from its parent" : "dismissed"
            
            DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: { [weak self] in
                assert(self == nil, "\(type) not deallocated after being \(disappearanceSource)")
            })
        }
    }
    
    private var dch_rootParentViewController: UIViewController {
        var root = self
        
        while let parent = root.parent {
            root = parent
        }
        
        return root
    }
    
}

extension UIColor {
    
    static func colorGreen() -> UIColor {
        return UIColor(red: 39/255.0, green: 134/255.0, blue: 93/255.0, alpha: 1.0)
    }
    
    static func colorRed() -> UIColor {
        return UIColor(red: 244/255.0, green: 67/255.0, blue: 54/255.0, alpha: 1.0)
    }
    
    static func colorGrey() -> UIColor {
        return UIColor(red: 241/255.0, green: 241/255.0, blue: 241/255.0, alpha: 1.0)
    }
    
    static func colorBorderGrey() -> UIColor {
        return UIColor(red: 216/255.0, green: 216/255.0, blue: 216/255.0, alpha: 1.0)
    }
    
    static func colorBorderButtonGrey() -> UIColor {
        return UIColor(red: 114/255.0, green: 117/255.0, blue: 124/255.0, alpha: 1.0)
    }
    
    static func colorBorderUITextField() -> UIColor {
        return UIColor(red: 190/255.0, green: 190/255.0, blue: 190/255.0, alpha: 1.0)
    }
    
    static func colorPedidoRealizado() -> UIColor {
        return UIColor(red: 76/255.0, green: 175/255.0, blue: 80/255.0, alpha: 1.0)
    }
    
    static func colorAguardando() -> UIColor {
        return UIColor(red: 16/255.0, green: 132/255.0, blue: 198/255.0, alpha: 1.0)
    }
    
    static func colorCancelado() -> UIColor {
        return UIColor(red: 114/255.0, green: 117/255.0, blue: 124/255.0, alpha: 1.0)
    }
    
    static func colorEntregue() -> UIColor {
        return UIColor(red: 50/255.0, green: 176/255.0, blue: 97/255.0, alpha: 1.0)
    }
    
    static func colorLogradouro() -> UIColor {
        return UIColor(red: 51/255.0, green: 56/255.0, blue: 65/255.0, alpha: 1.0)
    }
    
    static func colorMunicipio() -> UIColor {
        return UIColor(red: 142/255.0, green: 142/255.0, blue: 142/255.0, alpha: 1.0)
    }
    
    static func colorRedPage() -> UIColor {
        return UIColor(red: 255/255.0, green: 179/255.0, blue: 188/255.0, alpha: 1.0)
    }
    
}

extension UIButton {
    
    func addBlurEffect(style:UIBlurEffectStyle) {
        
        let effect = UIBlurEffect(style: style)
        let blur = UIVisualEffectView(effect: effect)
        blur.isUserInteractionEnabled = false
        self.insertSubview(blur, at: 0)
        
    }
    
    func addBorderAndColor(color:UIColor, border:CGFloat) {
        
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = border
        
    }
}

extension UITextField {
    func addBlur(style:UIBlurEffectStyle, view:UIView) {
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            let effect = UIBlurEffect(style: style)
            let blurEffectView = UIVisualEffectView(effect: effect)
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.backgroundColor = UIColor.clear
            
            self.background = view.imageWithView()
        }
    }
    
    func checkEmail() -> Bool {
        let valor = self.text!
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: valor, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, valor.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    func unmask() -> String! {
        var textOfField = self.text!
        textOfField = textOfField.replacingOccurrences(of: ".", with: "")
        textOfField = textOfField.replacingOccurrences(of: "-", with: "")
        textOfField = textOfField.replacingOccurrences(of: "(", with: "")
        textOfField = textOfField.replacingOccurrences(of: ")", with: "")
        return textOfField
    }
    
    func leftPadding(toLength: Int, withPad character: Character) -> String {
        let valor = self.text!
        //        let newLength = self.text?.characters.count
        let newLength = valor.characters.count
        if newLength < toLength {
            return String(repeatElement(character, count: toLength - newLength)) + valor
        }else{
            //return self.text!.substring(from: index(self.text!.startIndex, offsetBy: newLength - toLength))
            return valor.substring(from: valor.index(valor.startIndex, offsetBy: newLength - toLength))
        }
    }
    
    func checkUF() -> Bool {
        
        let textUF = self.text!
        let ufUpper = textUF.uppercased()
        let ufArray = ["AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RO", "RS", "RR", "SC", "SE", "SP", "TO"]
        
        let value = ufArray.contains(ufUpper)
        return value
        
    }
}

extension UITableView {
    func hideEmptyLines() {
        self.tableFooterView = UIView()
    }
}

extension UITableViewCell {
    
    func addSeparatorLineToTop(color:UIColor,height:CGFloat){
        let lineFrame = CGRect(x: 0, y: 0, width: bounds.size.width, height: height)
        let line = UIView(frame: lineFrame)
        line.backgroundColor = color
        addSubview(line)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UITableViewCell.dismissKeyboard))
//        view.addGestureRecognizer(tap)
        self.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
//        view.endEditing(true)
        self.endEditing(true)
    }
}

extension UIView {
    func imageWithView() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension UIImageView{
    
    func asCircle(){
        self.layer.cornerRadius = self.frame.width / 2;
        self.layer.masksToBounds = true
    }
    
    func asCircleColor(color:UIColor) {
        self.layer.cornerRadius = self.frame.width / 2;
        self.layer.borderWidth = 1.0
        self.layer.borderColor = color.cgColor
        self.layer.masksToBounds = true
    }
    
}

public extension UIImage {
    
    static func croppedImage(image:UIImage, cropRect: CGRect) -> UIImage{
        
        UIGraphicsBeginImageContext(cropRect.size)
        image.draw(at: CGPoint.init(x: -cropRect.origin.x, y: -cropRect.origin.y))
        let cropped = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return cropped!
    }
    
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat, up:Bool) -> UIImage {
        
        var position:CGFloat?
        
        if up {
            position = size.height - size.height
        }else{
            position = size.height - lineWidth
        }
        
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()        
        UIRectFill(CGRect(origin: CGPoint(x: 0,y :position!), size: CGSize(width: size.width, height: lineWidth)))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

extension String {
    
    subscript (i: Int) -> Character {
        return self[self.characters.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = characters.index(startIndex, offsetBy: r.lowerBound)
        let end = characters.index(start, offsetBy: r.upperBound - r.lowerBound)
        return self[Range(start ..< end)]
    }
    
    func lenght() -> Int {
        return self.characters.count
    }
    
    func encodeUrl() -> String {
        var retorn = self
        retorn = retorn.replacingOccurrences(of: " ", with: "%20")
        return retorn
    }
    
    func maskCPF() -> String {
        
        let valor = self
        let v1    = valor.substring(with: 0..<3)
        let v2    = valor.substring(with: 3..<6)
        let v3    = valor.substring(with: 6..<9)
        let v4    = valor.substring(from: 9)
        
        let cpfMask = "\(v1).\(v2).\(v3)-\(v4)"
        
        return cpfMask
    }
    
    func maskCEP() -> String {
        
        let valor = self
        let v1    = valor.substring(to: 5)
        let v2    = valor.substring(from: 5)
        
        let cepMask = "\(v1)-\(v2)"
        
        return cepMask
        
    }
    
    func maskPhone(celular:Bool) -> String{
        
        var valor = self
        let ddd = valor.substring(to: 2)
        var n1  = ""
        var n2  = ""
        
        
        if(celular){
            //81981092499
            n1  = valor.substring(with: 2..<7)
            n2  = valor.substring(from: 7)
            
        }else{
            //8134447957
            n1  = valor.substring(with: 2..<6)
            n2  = valor.substring(from: 6)
        }
        
        valor = "(\(ddd)) \(n1)-\(n2)"
        return valor
    }
    
    func unmask() -> String! {
        var textOfField = self
        textOfField = textOfField.replacingOccurrences(of: ".", with: "")
        textOfField = textOfField.replacingOccurrences(of: "-", with: "")
        textOfField = textOfField.replacingOccurrences(of: "(", with: "")
        textOfField = textOfField.replacingOccurrences(of: ")", with: "")
        textOfField = textOfField.replacingOccurrences(of: "+", with: "")
        textOfField = textOfField.replacingOccurrences(of: " ", with: "")
        textOfField = textOfField.replacingOccurrences(of: "/", with: "")
        return textOfField
    }
    
    func separarDDD_CELULAR() -> [String:String] {
        
        var array:[String:String]?
        var value = self
        value = value.unmask()
        let ddd = value.substring(to: 2)
        let phone = value.substring(from: 2)
        
        array = ["ddd":ddd,
                 "phone":phone]
        
        return array!
        
    }
    
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let label: UILabel = UILabel()
        
        label.text = self
        label.numberOfLines = 0;
        label.font = font
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        let size: CGSize = label.sizeThatFits(CGSize(width: width, height: 1000))
        
        return size.height
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    func leftPadding(toLength: Int, withPad character: Character) -> String {
        let newLength = self.characters.count
        if newLength < toLength {
            return String(repeatElement(character, count: toLength - newLength)) + self
        }else{
            return self.substring(from: index(self.startIndex, offsetBy: newLength - toLength))
        }
    }
    
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
    
    var isCPF: Bool {
        
        let cpf = self.replacingOccurrences(of: ".", with: "").replacingOccurrences(of: "-", with: "")
        
        if cpf.characters.count == 11 {
            
            let d1 = Int(cpf.substring(with: Range(cpf.characters.index(cpf.startIndex, offsetBy: 9) ..< cpf.characters.index(cpf.startIndex, offsetBy: 10))))
            let d2 = Int(cpf.substring(with: Range(cpf.characters.index(cpf.startIndex, offsetBy: 10) ..< cpf.characters.index(cpf.startIndex, offsetBy: 11))))
            
            var temp1 = 0, temp2 = 0
            
            for i in 0...8 {
                
                let char = Int(cpf.substring(with: Range(cpf.characters.index(cpf.startIndex, offsetBy: i) ..< cpf.characters.index(cpf.startIndex, offsetBy: i + 1))))
                
                temp1 += char! * (10 - i)
                temp2 += char! * (11 - i)
                
            }
            
            temp1 %= 11
            temp1 = temp1 < 2 ? 0 : 11-temp1
            
            temp2 += temp1 * 2
            temp2 %= 11
            temp2 = temp2 < 2 ? 0 : 11-temp2
            
            if temp1 == d1 && temp2 == d2 {
                
                return true
                
            }
        }
        
        return false
    }
    
    var isUF : Bool {
        
        let ufUpper = self.uppercased()
        let ufArray = ["AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RO", "RS", "RR", "SC", "SE", "SP", "TO"]
        
        let value = ufArray.contains(ufUpper)
        return value
        
    }
    
    var isDate : Bool {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if (dateFormatter.date(from: self) != nil){
            return true
        }
        
        return false
        
    }
    
    var isOfAge : Bool {
        
        if self.calculaIdade >= 18 {
            return true
        }
        
        return false
    }
    
    var calculaIdade : Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
        let now = NSDate()
        let startDate = dateFormatter.date(from: self)
        let calendar : NSCalendar = NSCalendar.current as NSCalendar
        let ageComponents = calendar.components(.year, from: startDate!, to: now as Date, options: [])
        let age = ageComponents.year!
        return age
    }
    
    func convertToTimestamp() -> Int {
        
        let dfmatter = DateFormatter()
        dfmatter.dateFormat="dd/MM/yyyy"
        let date = dfmatter.date(from: self)
        let dateStamp:TimeInterval = date!.timeIntervalSince1970
        let dateSt:Int = Int(dateStamp * 1000)
        
        return dateSt
        
    }
    
    func convertStringToDictionary() -> [String: AnyObject]? {
        
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String : AnyObject]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
        
    }
    
    
    func getIcon() -> String {
        var nomeDep:String = self
        if(nomeDep.lowercased() == "ar e ventilacao") {
            return "ic_fan_24dp"
        }
        else if(nomeDep.lowercased() == "audio") {
            return "ic_speaker_24dp"
        }
        else if(nomeDep.lowercased() == "automotivo") {
            return "ic_car_24dp"
        }
        else if(nomeDep.lowercased() == "cameras e filmadora") {
            return "ic_camcorder_24dp"
        }
        else if(nomeDep.lowercased() == "eletrodomestico") {
            return "ic_eletro_24dp"
        }
        else if(nomeDep.lowercased() == "eletroportateis") {
            return "ic_radio_24dp"
        }
        else if(nomeDep.lowercased() == "esporte") {
            return "ic_soccer_24dp"
        }
        else if(nomeDep.lowercased() == "ferramentas") {
            return "ic_ferramenta_24dp"
        }
        else if(nomeDep.lowercased() == "informatica") {
            return "ic_informatica_24dp"
        }
        else if(nomeDep.lowercased() == "moveis") {
            return "ic_moveis_24dp"
        }
        else if(nomeDep.lowercased() == "saude e beleza") {
            return "ic_saude_beleza_24dp"
        }
        else if(nomeDep.lowercased() == "telefonia") {
            return "ic_telefonia_24dp"
        }
        else if(nomeDep.lowercased() == "tv e video") {
            return "ic_television_24dp"
        }
        else if(nomeDep.lowercased() == "utilidades domesticas") {
            return "ic_util_dom_24dp"
        }
        else if(nomeDep.lowercased() == "video game") {
            return "ic_gamepad_24dp"
        }
        else {
            return "ic_cube_outline_black_24dp"
        }
        
    }
}
extension Int {
    
    func convertToDate() -> String {
        
        let dateTimeStamp = NSDate(timeIntervalSince1970:Double(self)/1000)  //UTC time
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.local //Edit
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        
        let strDateSelect = dateFormatter.string(from: dateTimeStamp as Date)
        return strDateSelect
        
    }
    
    var second: DateComponents {
        var components = DateComponents()
        components.second = self;
        return components
    }
    
    var seconds: DateComponents {
        return self.second
    }
    
    var minute: DateComponents {
        var components = DateComponents()
        components.minute = self;
        return components
    }
    
    var minutes: DateComponents {
        return self.minute
    }
    
    var hour: DateComponents {
        var components = DateComponents()
        components.hour = self;
        return components
    }
    
    var hours: DateComponents {
        return self.hour
    }
    
    var day: DateComponents {
        var components = DateComponents()
        components.day = self;
        return components
    }
    
    var days: DateComponents {
        return self.day
    }
    
    var week: DateComponents {
        var components = DateComponents()
        components.weekOfYear = self;
        return components
    }
    
    var weeks: DateComponents {
        return self.week
    }
    
    var month: DateComponents {
        var components = DateComponents()
        components.month = self;
        return components
    }
    
    var months: DateComponents {
        return self.month
    }
    
    var year: DateComponents {
        var components = DateComponents()
        components.year = self;
        return components
    }
    
    var years: DateComponents {
        return self.year
    }
    
}
extension DateComponents {
    
    var fromNow: Date {
        return Calendar.current.date(byAdding: self,
                                     to: Date())!
    }
    
    var ago: Date {
        let value = self
        return Calendar.current.date(byAdding: value,
                                     to: Date())!
    }
    
}
extension Float {
    
    func formatCurrency() -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.maximumFractionDigits = 2
        formatter.locale = Locale(identifier: Locale.current.identifier)
        let result = formatter.string(from: self as NSNumber)
        return result!
    }
}
extension UInt {
    func mesDoisDigitos() -> String{
        var mes:String?
        let value = self
        if value < 10 {
            mes = "0\(value)"
        }else{
            mes = String(describing: value)
        }
        return mes!
    }
}
extension MutableCollection where Indices.Iterator.Element == Index {
    /// Shuffles the contents of this collection.
    mutating func shuffle() {
        let c = count
        guard c > 1 else { return }
        
        for (firstUnshuffled , unshuffledCount) in zip(indices, stride(from: c, to: 1, by: -1)) {
            let d: IndexDistance = numericCast(arc4random_uniform(numericCast(unshuffledCount)))
            guard d != 0 else { continue }
            let i = index(firstUnshuffled, offsetBy: d)
            swap(&self[firstUnshuffled], &self[i])
        }
    }
}

extension Sequence {
    /// Returns an array with the contents of this sequence, shuffled.
    func shuffled() -> [Iterator.Element] {
        var result = Array(self)
        result.shuffle()
        return result
    }
}

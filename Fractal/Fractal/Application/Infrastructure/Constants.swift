//
//  Constants.swift
//  Fractal
//
//  Created by Anderson Silva on 20/09/17.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import Foundation

let BEER_LIST = "https://api.punkapi.com/v2/beers/"
let BEER_PAGE = "https://api.punkapi.com/v2/beers?page=2&per_page=80"

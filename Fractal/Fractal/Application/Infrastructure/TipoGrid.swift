//
//  TipoGrid.swift
//  ArmazemPB-iOS
//
//  Created by Anderson Silva on 23/08/17.
//  Copyright © 2017 Wyllamys Cavalcante. All rights reserved.
//

import Foundation

enum TipoGrid : String {
    case GRID
    case BIG
    case LIST
}

//
//  Texts.swift
//  ArmazemPB-iOS
//
//  Created by Anderson Silva on 11/07/17.
//  Copyright © 2017 Wyllamys Cavalcante. All rights reserved.
//

import Foundation

//GOOGLE MAPS
let API_KEY = "AIzaSyAnmPr50MR9_1ydzqyMOslGm4sAqcL7IJE"

//VARIAVEIS GLOBIAS
let keyIdUser = "idUser"
let keyNomeUser = "nomeUser"
let keyEmailUser = "emailUser"
let keyCognitoId = "cognitoId"
let keyOpenIdToken = "openIdToken"
let keySaltedHashToken = "saltedHashToken"
let keyTokenFacebook = "tokenFacebook"
let keyNameFacebook = "nameFacebook"
let keyUidFacebook = "idFacebook"
let keyEmailFacebook = "emailFacebook"
let keyCadastroFacebook = "cadastroFacebook"
let poolKey = "poolKey"
let keyAccessKey = "accessKey"
let keySecretKey = "secretKey"
let keySessionKey = "sessionKey"
let keyPedidos = "pedidosKey"

let DEVELOPER_PROVIDER_NAME = "cognito-identity.amazonaws.com"
let IMG_PLACEHOLDER = "logo_placeholder"
let IMG_PLACEHOLDER_COLOR = "logo_armazem"


//SEGUES
let sgUserCadastro = "userCadastro"
let sgCadastroUserMeuArmazem = "sgCadastroUserMeuArmazem"
let sgMeusEnderecos = "sgMeusEnderecos"
let sgMeusPedidos = "sgMeusPedidos"
let sgEnderecoCEP = "sgEnderecoCEP"
let sgEnderecoEdit = "sgEnderecoEdit"
let sgPedidosDetalhes = "sgPedidosDetalhes"
let sgEditAddress = "sgEditAddress"
let sgPoliticaPrivacidade = "sgPoliticaPrivacidade"
let sgCartoes = "sgCartoes"
let sgAlterarSenha = "sgAlterarSenha"
let sgCadastrarCartao = "sgCadastrarCartao"
let sgNotificacoes = "sgNotificacoes"
let sgNossasLojas = "sgNossasLojas"
let sgPerguntas = "sgPerguntas"
let sgProdutos = "sgProdutos"
let sgImgGaleria = "sgImgGaleria"
let sgCesta = "sgCesta"
let sgFormaPag = "sgFormaPag"
let sgFormaEndereco = "sgFormaEndereco"
let sgNewAddresPag = "sgNewAddresPag"
let sgNewCardPag = "sgNewCardPag"
let sgRevisaoPedido = "sgRevisaoPedido"
let sgConfirmaPedido = "sgConfirmaPedido"
let sgParcelasProduto = "sgParcelasProduto"
let sgDisponibilidadeProdutos = "sgDisponibilidadeProdutos"
let sgResultadoSearch = "sgResultadoSearch"
let sgOrdenacao = "sgOrdenacao"
let sgDetailProdutoSearch = "sgDetailProdutoSearch"
let sgNegativaPedido = "sgNegativaPedido"
let sgCaracteristicasProd = "sgCaracteristicasProd"
let sgDisponibilidade = "sgDisponibilidade"
let sgDisponibilidadePag = "sgDisponibilidadePag"
let sgEfetuarCadastroCesta = "sgEfetuarCadastroCesta"

// ----   Generic
let NO_CONNECTION = "Falha ao conectar à internet"
let PLEASE_FULFILL_TEXTFIELD = "Por favor preencha o campo"
let WARNING = "Atenção"
let INITIAL_PAGE = "PÁGINA INICIAL"
let GENERIC_ERROR = "Não foi possível exibir as informações. Tente novamente"
let DONE = "PRONTO!"
let CANCEL = "Cancelar"
let OK = "Ok"
let OPS = "Armazém Paraíba, informa:"
let ARMAZEM_TITLE = "Armazém Paraíba, informa:"

// ----   Login
let CANCEL_USER_FACEBOOK = "Login cancelado pelo usuário."
let CPF_OFF_NETWORK = "Não foi possível carregar as informações. Verifique sua conexão ou tente novamente."
let DADOS_INVALIDOS = "Dados inválidos!"
let EMAIL_INVALIDO  = "E-mail inválido!"
let CAMPOS_VAZIO    = "Por favor, preencha os campos obrigatórios."
let CPF_INVALID = "O CPF que você informou está incorreto. Verifique o número e tente novamente."
let LOGIN_HAVE_EXPIRATED = "Sua sessão foi expirada, por favor logue novamente."
let WRONG_ADDRESS = "O número do endereço está incorreto!"
let INFORM_YOUR_PHONE = "Por favor, infome seu número de telefone"
let INFORM_YOUR_CODE = "Por favor informe o código verificador"
let PHONE_VERIFY_SUCCESS = "Telefone confirmado com sucesso."
let PHONE_VERIFY_FAILED = "Código inválido, por favor tente novamente."
let PHONE_SMS_VERIFY = "O código que você digitou é diferente do que enviamos via SMS. Por favor, tente novamente."
let ID_POS_ERROR = "Você não respondeu às perguntas corretamente.\nTente novamente."
let ID_POS_SMS_ERROR = "Código inválido, por favor tente novamente."
let PHONE_NOT_FOUND = "Não conseguimos mandar o código agora, por favor tente mais tarde."
let TEMPORARY_BLOCKED = "Seu acesso está bloqueado temporariamente."
let LOG_OUT = "Você deseja realmente sair?"
let CONFIRM_LOGOUT = "Sim, desejo sair"
let CHECK_LOGIN = "Você precisa estar logado para continuar."

// ----   Esqueceu a senha
let TITLE_ESQUECEU_SENHA = "Esqueceu a senha?"
let MSG_ESQUECEU_SENHA   = "Informe seu e-mail e nós lhe enviaremos um e-mail para cadastrar sua nova senha."
let BTN_ESQUECEU_SENHA   = "Enviar e-mail"

// ----   Cadastro User
let CADASTRO_SUCESSO = "Cadastro realizado com sucesso!"
let CAMPO_OBRIGATORIO = "Campo Obrigatório!"
let CAMPO_CPF = "Por favor, preencha o campo CPF."
let CAMPO_NOME = "Por favor, preencha o campo NOME COMPLETO."
let CAMPO_DT = "Por favor, preencha o campo DATA DE NASCIMENTO."
let CAMPO_DT_INVALIDO = "DATA INVÁLIDA!"
let CAMPO_DT_MENOR_IDADE = "Você deve ter pelo menos 18 anos para se cadastrar."
let CAMPO_CEL = "Por favor, preencha o campo CELULAR."
let CAMPO_EMAIL = "Por favor, preencha o campo E-MAIL."
let CAMPO_SENHA = "Por favor, preencha o campo SENHA."
let CAMPO_CONFIR = "Por favor, preencha o campo CONFIRMAR SENHA."
let CAMPO_GENERO = "Por favor, selecione um dos genêros."
let SENHA_COUNT  = "Campo senha exige no mínimo 6 caracteres."
let SENHA_DIFERENTE = "Os valores dos campos SENHA e CONFIRMAR SENHA não confere, favor verificar."

// ----   CARTÕES
let MSG_DELETE_CARD = "Deseja realmente excluir este cartão?"
let CAMPO_NUMERO_CARD = "Por favor, preencha o campo NÚMERO."
let CAMPO_NUMERO_CARD_INVALIDO = "Número do cartão inválido!"
let CAMPO_VALIDADE_CARD = "Por favor, preencha o campo VALIDADE."
let CAMPO_NOME_CARD = "Por favor, preencha o campo NOME."
let CAMPO_CVV_CARD = "Por favor, preencha o campo CVV."
let CVV_INVALID    = "CVV INVÁLIDO!"
let CAMPO_PARCELAS = "Por favor, selecione a quantidade de parcelas que deseja realizar a compra."

// ----   Cadastro Endereço
let CAMPO_CEP = "Por favor, preencha o campo CEP."
let CEP_INVALIDO = "CEP inválido!"
let CAMPO_ENDERECO = "Por favor, preencha o campo ENDEREÇO."
let CAMPO_BAIRRO = "Por favor, preencha o campo BAIRRO."
let CAMPO_CIDADE = "Por favor, preencha o campo CIDADE."
let CAMPO_UF = "Por favor, preencha o campo UF."
let UF_INVALID = "Ops! O UF que você informou está incorreto. Verifique o número e tente novamente."
let CAMPO_NUMERO = "Por favor, preencha o campo NÚMERO."
let DELETE_ENDERECO = "Você deseja realmente excluir este endereço?"
let CONFIRM_DELETE_ENDERECO = "Sim, desejo excluir."

// ----   URLS
let URL_POLITICAS = "http://armazempb.com.br/politica/indexIOS.html"
let URL_PERGUNTAS_FREQUENTES = "https://d1rybiypffpn9g.cloudfront.net/atendimento/index.html"

// ----   HOME
let PLACEHOLDER_SEARCH_HOME = "Procure no Armazém Paraíba"

//
//  BaseViewController.swift
//  ArmazemPB-iOS
//
//  Created by Anderson Silva on 11/07/17.
//  Copyright © 2017 Wyllamys Cavalcante. All rights reserved.
//

import UIKit
import Kingfisher

protocol BaseViewInterface : class {
    func loadingShow()
    func loadingHide()
    func loadingLoginShow()
    func loadingLoginHide()
    func showViewError(title: String, msg: String, btn: String)
    func showAlertPopViewController(title:String, msg:String, btn:String)
    func showAlertPopRootViewController(title:String, msg:String, btn:String, animated:Bool)
    func showAlertModal(title:String, msg:String, btn:String)
    func showNextVC(identifier:String, _ sender:Any?)
    func popViewController()
    func popToViewController(_ vc:UIViewController, _ animated:Bool)
    func popToRootViewController(_ animated:Bool)
    func dismissModal()
}

class BaseViewController: UIViewController {

    var loadingView : UIViewController?
    let searchBar = UISearchBar()
    
    let appSession = AppSession.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let recognizer:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(BaseViewController.backButton))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addBackButton() {
        let barButton = UIBarButtonItem(image: UIImage(named: "ic_back"), style: UIBarButtonItemStyle.plain, target: self , action: #selector(BaseViewController.backButton))
        barButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = barButton
    }
    
    func backButton(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func addBackModalRight() {
        let barButton = UIBarButtonItem(image: UIImage(named: "ic_signup_close"), style: UIBarButtonItemStyle.plain, target: self , action: #selector(BaseViewController.backButtonModal))
        barButton.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = barButton
    }
    
    func addBackModal() {
        let barButton = UIBarButtonItem(image: UIImage(named: "ic_signup_close"), style: UIBarButtonItemStyle.plain, target: self , action: #selector(BaseViewController.backButtonModal))
        barButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = barButton
    }
    
    func backButtonModal(animated:Bool=true) {
        self.dismiss(animated: animated, completion: nil)
    }
    
    func backViewTabbar(_ view:Int=0) {
        self.tabBarController?.selectedIndex = view
    }
    
    func setBorderColorButton(button:UIButton, color:UIColor){
        button.layer.borderWidth = 1
        button.layer.borderColor = color.cgColor
    }
    
    func alertMensagem(title:String, msg:String, btn:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: btn, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func disableView(enable:Bool) {
        self.view.isUserInteractionEnabled = enable
        self.navigationItem.leftBarButtonItem?.isEnabled = enable
        self.navigationItem.rightBarButtonItem?.isEnabled = enable
    }
    
    func showLoading() {
        DispatchQueue.main.async {
//            self.disableView(enable: false)
            self.loadingView = UIStoryboard(name: "Loading", bundle: nil).instantiateViewController(withIdentifier: "LoadingView")
            self.loadingView!.view.alpha = 0.0
            self.view.addSubview(self.loadingView!.view)
            
            UIView.animate(withDuration: 0.5, animations: {
                self.loadingView!.view.alpha = 0.8
            })
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.8, animations: {
                self.loadingView!.view.alpha = 0.0
            }, completion: { (finished) in
                if finished {
                    self.loadingView?.view.removeFromSuperview()
                    self.loadingView = nil
//                    self.disableView(enable: true)
                }
            })
        }
    }
    
    func showLoadingLogin() {
        DispatchQueue.main.async {
            self.loadingView = UIStoryboard(name: "Loading", bundle: nil).instantiateViewController(withIdentifier: "LoadingView")
            self.loadingView!.view.alpha = 0.0
            self.view.addSubview(self.loadingView!.view)
            
            UIView.animate(withDuration: 0.5, animations: {
                self.loadingView!.view.alpha = 0.8
            })
        }
    }
    
    func hideLoadingLogin() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.8, animations: {
                self.loadingView!.view.alpha = 0.0
            }, completion: { (finished) in
                if finished {
                    self.loadingView?.view.removeFromSuperview()
                    self.loadingView = nil
                }
            })
        }
    }
    
    func tapDismiss(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func addBlurArea(area: UIView, style: UIBlurEffectStyle) {
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = area.bounds
        self.view.insertSubview(blurEffectView, at: 0)
        
    }
    
    
    func getTelaLogin() {
        
        let story = UIStoryboard(name: "Login", bundle: nil)
        let vc    = story.instantiateViewController(withIdentifier: "loginViewController")
        let nav   = UINavigationController(rootViewController: vc)
        nav.navigationBar.barTintColor = UIColor.colorRed()
        nav.navigationBar.isTranslucent = false
//        nav.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 20.0, weight: UIFontWeightMedium), NSForegroundColorAttributeName : UIColor.white]
        present(nav, animated: true, completion: nil)
    }
    
    
    func showViewModal(name:String, identifier:String, animated:Bool) {
        let story = UIStoryboard(name: name, bundle: nil)
        let vc    = story.instantiateViewController(withIdentifier: identifier)
        let nav   = UINavigationController(rootViewController: vc)
        nav.navigationBar.barTintColor = UIColor.colorRed()
        nav.navigationBar.isTranslucent = false
        present(nav, animated: animated, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BaseViewController : BaseViewInterface {
    
    func popViewController() {
        self.backButton()
    }
    
    func popToViewController(_ vc: UIViewController, _ animated: Bool) {
        self.navigationController?.popToViewController(vc, animated: animated)
    }
    
    func popToRootViewController(_ animated: Bool) {
        self.navigationController?.popToRootViewController(animated: animated)
    }
    
    func showNextVC(identifier: String, _ sender: Any?) {
        self.performSegue(withIdentifier: identifier, sender: sender)
    }
    
    func dismissModal() {
        self.backButtonModal(animated: true)
    }
    
    func loadingLoginShow(){
        self.showLoadingLogin()
    }
    func loadingLoginHide(){
        self.hideLoadingLogin()
    }

    func loadingShow() {
        self.showLoading()
    }
    
    func loadingHide() {
        self.hideLoading()
    }
    
    func showViewError(title: String, msg: String, btn: String) {
        self.alertMensagem(title: title, msg: msg, btn: btn)
    }
    
    func showAlertPopRootViewController(title:String, msg:String, btn:String, animated:Bool) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: btn, style: .default, handler: { (action) in
            self.popToRootViewController(animated)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertPopViewController(title:String, msg:String, btn:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: btn, style: .default, handler: { (action) in
            self.backButton()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertModal(title:String, msg:String, btn:String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: btn, style: .default, handler: { (action) in
            self.backButtonModal(animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

//
//  ButtonStyle.swift
//  ArmazemPB-iOS
//
//  Created by Anderson Silva on 15/08/17.
//  Copyright © 2017 Wyllamys Cavalcante. All rights reserved.
//

import UIKit

@IBDesignable
class ButtonStyle : UIButton {
    @IBInspectable var cornerRadius : CGFloat = 0{
        didSet{
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable var borderWidth : CGFloat = 0 {
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var borderColor : UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
}


//
//  BeersViewController.swift
//  Fractal
//
//  Created by Anderson Silva on 20/09/17.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit

class BeersViewController: BaseViewController {

    @IBOutlet weak var tbView: UITableView!
    var beersManager = BeersManager()
    var objBeers:[Beers]?
    var total = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.beersManager.baseInterface = self
        self.beersManager.beersInterface = self
        self.beersManager.getBeers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadXib() {
        let celulas = UINib(nibName: "BeersTableViewCell", bundle: nil)
        self.tbView.register(celulas, forCellReuseIdentifier: "beersCell")
        
        self.tbView.tableFooterView = UIView()
        self.view.addSubview(self.tbView)
        self.tbView.reloadData()
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sgDetail" {
            let vc = segue.destination as! BeersDetailViewController
            vc.objBeer = sender as! Beers
        }
        
    }
 

}
extension BeersViewController : UITableViewDelegate, UITableViewDataSource, BeersTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "beersCell", for: indexPath) as! BeersTableViewCell
        cell.delegate = self
        cell.setup(beer: self.objBeers![indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "sgDetail", sender: self.objBeers?[indexPath.row])
    }
    
    func setLike(in cell: BeersTableViewCell) {
        if let indexPath = self.tbView.indexPath(for: cell) {
            let indexes = self.appSession.likes.enumerated().filter({ $0.element.beers.id == self.objBeers![indexPath.row].id })
            if indexes.isEmpty {
                self.appSession.likes.append(LikesBeers(beers: self.objBeers![indexPath.row], like: true))
                cell.btLike.setImage(UIImage(named: "ic_heart_ati1"), for: .normal)
            }else{
                self.appSession.likes.remove(at: indexes[0].offset)
                cell.btLike.setImage(UIImage(named: "ico_heart_des"), for: .normal)
            }
        }
    }
}
extension BeersViewController : BeersInterface {
    func updateBeers(beers: [Beers]) {
        self.total = beers.count
        self.objBeers = beers
        self.loadXib()
    }
}

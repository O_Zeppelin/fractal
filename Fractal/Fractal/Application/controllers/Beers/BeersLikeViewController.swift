//
//  BeersLikeViewController.swift
//  Fractal
//
//  Created by Anderson Silva on 20/09/17.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit

class BeersLikeViewController: BaseViewController {

    @IBOutlet weak var tbView: UITableView!
    
    var objBeers:[LikesBeers]?
    var total = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.appSession.likes.count > 0 {
            self.total = self.appSession.likes.count
            self.objBeers = self.appSession.likes
            loadXib()
            self.tbView.isHidden = false
        }else{
            self.tbView.isHidden = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    func loadXib() {
        let celulas = UINib(nibName: "BeersTableViewCell", bundle: nil)
        self.tbView.register(celulas, forCellReuseIdentifier: "beersCell")
        
        self.tbView.tableFooterView = UIView()
        self.view.addSubview(self.tbView)
        self.tbView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BeersLikeViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "beersCell", for: indexPath) as! BeersTableViewCell
        cell.btLike.isHidden = true
        cell.setup(beer: self.objBeers![indexPath.row].beers)
        return cell
    }
    
}

//
//  BeersDetailViewController.swift
//  Fractal
//
//  Created by Anderson Silva on 23/09/17.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit
import Kingfisher

class BeersDetailViewController: BaseViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbTitulo: UILabel!
    @IBOutlet weak var lbTagline: UILabel!
    @IBOutlet weak var lbDescricao: UITextView!
    
    var objBeer:Beers!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadData()
    }

    func loadData() {
        
        if let photo = objBeer.imageUrl {
            self.img.kf.setImage(with: URL(string: photo))
        }
        
        self.lbTitulo.text = self.objBeer.name
        self.lbTagline.text = self.objBeer.tagline
        self.lbDescricao.text = self.objBeer.descriptionValue
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

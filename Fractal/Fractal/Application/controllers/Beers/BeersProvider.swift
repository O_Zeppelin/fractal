//
//  BeersProvider.swift
//  Fractal
//
//  Created by Anderson Silva on 20/09/17.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class BeersProvider : NSObject {
    
    func getBeerList(completion:@escaping(_ beers:[Beers]?, _ error:Error?) -> Void) -> Void {
        
        request(BEER_LIST, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseArray { (response: DataResponse<[Beers]>) in
                switch response.result {
                case .success(let beers): completion(beers, nil); break;
                case .failure(let error): completion(nil, error); break;
                }
        }
        
    }
    
}

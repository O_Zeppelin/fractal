//
//  BeersManager.swift
//  Fractal
//
//  Created by Anderson Silva on 20/09/17.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import Foundation

protocol BeersInterface : class{
    func updateBeers(beers:[Beers])
}

class BeersManager : NSObject {
    
    private lazy var business : BeersBusiness = {
        return BeersBusiness.init()
    }()
    
    weak var beersInterface : BeersInterface?
    weak var baseInterface : BaseViewInterface?
    
    func getBeers() {
        
        self.baseInterface?.loadingShow()
        business.getBeerList { (beers, error) in
            guard beers != nil else {
                self.baseInterface?.showAlertModal(title: "APP INFORMA", msg: error!, btn: OK)
                self.baseInterface?.loadingHide()
                return
            }
            self.baseInterface?.loadingHide()
            self.beersInterface?.updateBeers(beers: beers!)
        }
        
    }
    
}

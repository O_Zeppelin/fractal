//
//  BeersBusiness.swift
//  Fractal
//
//  Created by Anderson Silva on 20/09/17.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import Foundation

class BeersBusiness : NSObject {
    
    private lazy var provider:BeersProvider = {
        return BeersProvider.init()
    }()
    
    func getBeerList(completion:@escaping(_ beers:[Beers]?, _ error:String?) -> Void) -> Void {
        
        provider.getBeerList { (beers, error) in
            guard beers != nil else {
                completion(nil, error?.localizedDescription)
                return
            }
            completion(beers, nil)
        }
        
    }
    
    
}


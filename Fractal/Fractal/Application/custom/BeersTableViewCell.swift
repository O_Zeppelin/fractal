//
//  BeersTableViewCell.swift
//  Fractal
//
//  Created by Anderson Silva on 20/09/17.
//  Copyright © 2017 Anderson Silva. All rights reserved.
//

import UIKit
import Kingfisher

protocol BeersTableViewCellDelegate : class {
    func setLike(in cell:BeersTableViewCell)
}

class BeersTableViewCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbText: UILabel!
    @IBOutlet weak var btLike: UIButton!
    
    weak var delegate : BeersTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(beer:Beers) {
        if let photo = beer.imageUrl {
            self.img.kf.setImage(with: URL(string: photo))
        }
        self.lbTitle.text = beer.name
        self.lbText.text  = beer.tagline
    }
    
    @IBAction func like(_ sender: UIButton) {
        self.delegate?.setLike(in: self)
    }
}

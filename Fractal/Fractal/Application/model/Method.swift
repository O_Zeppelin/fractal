//
//  Method.swift
//
//  Created by Anderson Silva on 20/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Method: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let fermentation = "fermentation"
    static let mashTemp = "mash_temp"
    static let twist = "twist"
  }

  // MARK: Properties
  public var fermentation: Fermentation?
  public var mashTemp: [MashTemp]?
  public var twist: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    fermentation <- map[SerializationKeys.fermentation]
    mashTemp <- map[SerializationKeys.mashTemp]
    twist <- map[SerializationKeys.twist]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = fermentation { dictionary[SerializationKeys.fermentation] = value.dictionaryRepresentation() }
    if let value = mashTemp { dictionary[SerializationKeys.mashTemp] = value.map { $0.dictionaryRepresentation() } }
    if let value = twist { dictionary[SerializationKeys.twist] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.fermentation = aDecoder.decodeObject(forKey: SerializationKeys.fermentation) as? Fermentation
    self.mashTemp = aDecoder.decodeObject(forKey: SerializationKeys.mashTemp) as? [MashTemp]
    self.twist = aDecoder.decodeObject(forKey: SerializationKeys.twist) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(fermentation, forKey: SerializationKeys.fermentation)
    aCoder.encode(mashTemp, forKey: SerializationKeys.mashTemp)
    aCoder.encode(twist, forKey: SerializationKeys.twist)
  }

}

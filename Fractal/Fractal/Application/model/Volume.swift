//
//  Volume.swift
//
//  Created by Anderson Silva on 20/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Volume: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let unit = "unit"
    static let value = "value"
  }

  // MARK: Properties
  public var unit: String?
  public var value: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    unit <- map[SerializationKeys.unit]
    value <- map[SerializationKeys.value]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = unit { dictionary[SerializationKeys.unit] = value }
    if let value = value { dictionary[SerializationKeys.value] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.unit = aDecoder.decodeObject(forKey: SerializationKeys.unit) as? String
    self.value = aDecoder.decodeObject(forKey: SerializationKeys.value) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(unit, forKey: SerializationKeys.unit)
    aCoder.encode(value, forKey: SerializationKeys.value)
  }

}

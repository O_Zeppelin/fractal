//
//  Beers.swift
//
//  Created by Anderson Silva on 20/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Beers: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let ingredients = "ingredients"
    static let srm = "srm"
    static let method = "method"
    static let name = "name"
    static let ebc = "ebc"
    static let contributedBy = "contributed_by"
    static let ibu = "ibu"
    static let targetOg = "target_og"
    static let targetFg = "target_fg"
    static let abv = "abv"
    static let volume = "volume"
    static let ph = "ph"
    static let boilVolume = "boil_volume"
    static let firstBrewed = "first_brewed"
    static let brewersTips = "brewers_tips"
    static let descriptionValue = "description"
    static let id = "id"
    static let foodPairing = "food_pairing"
    static let attenuationLevel = "attenuation_level"
    static let imageUrl = "image_url"
    static let tagline = "tagline"
  }

  // MARK: Properties
  public var ingredients: Ingredients?
  public var srm: Int?
  public var method: Method?
  public var name: String?
  public var ebc: Int?
  public var contributedBy: String?
  public var ibu: Int?
  public var targetOg: Int?
  public var targetFg: Int?
  public var abv: Float?
  public var volume: Volume?
  public var ph: Float?
  public var boilVolume: BoilVolume?
  public var firstBrewed: String?
  public var brewersTips: String?
  public var descriptionValue: String?
  public var id: Int?
  public var foodPairing: [String]?
  public var attenuationLevel: Int?
  public var imageUrl: String?
  public var tagline: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    ingredients <- map[SerializationKeys.ingredients]
    srm <- map[SerializationKeys.srm]
    method <- map[SerializationKeys.method]
    name <- map[SerializationKeys.name]
    ebc <- map[SerializationKeys.ebc]
    contributedBy <- map[SerializationKeys.contributedBy]
    ibu <- map[SerializationKeys.ibu]
    targetOg <- map[SerializationKeys.targetOg]
    targetFg <- map[SerializationKeys.targetFg]
    abv <- map[SerializationKeys.abv]
    volume <- map[SerializationKeys.volume]
    ph <- map[SerializationKeys.ph]
    boilVolume <- map[SerializationKeys.boilVolume]
    firstBrewed <- map[SerializationKeys.firstBrewed]
    brewersTips <- map[SerializationKeys.brewersTips]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    id <- map[SerializationKeys.id]
    foodPairing <- map[SerializationKeys.foodPairing]
    attenuationLevel <- map[SerializationKeys.attenuationLevel]
    imageUrl <- map[SerializationKeys.imageUrl]
    tagline <- map[SerializationKeys.tagline]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = ingredients { dictionary[SerializationKeys.ingredients] = value.dictionaryRepresentation() }
    if let value = srm { dictionary[SerializationKeys.srm] = value }
    if let value = method { dictionary[SerializationKeys.method] = value.dictionaryRepresentation() }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = ebc { dictionary[SerializationKeys.ebc] = value }
    if let value = contributedBy { dictionary[SerializationKeys.contributedBy] = value }
    if let value = ibu { dictionary[SerializationKeys.ibu] = value }
    if let value = targetOg { dictionary[SerializationKeys.targetOg] = value }
    if let value = targetFg { dictionary[SerializationKeys.targetFg] = value }
    if let value = abv { dictionary[SerializationKeys.abv] = value }
    if let value = volume { dictionary[SerializationKeys.volume] = value.dictionaryRepresentation() }
    if let value = ph { dictionary[SerializationKeys.ph] = value }
    if let value = boilVolume { dictionary[SerializationKeys.boilVolume] = value.dictionaryRepresentation() }
    if let value = firstBrewed { dictionary[SerializationKeys.firstBrewed] = value }
    if let value = brewersTips { dictionary[SerializationKeys.brewersTips] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = foodPairing { dictionary[SerializationKeys.foodPairing] = value }
    if let value = attenuationLevel { dictionary[SerializationKeys.attenuationLevel] = value }
    if let value = imageUrl { dictionary[SerializationKeys.imageUrl] = value }
    if let value = tagline { dictionary[SerializationKeys.tagline] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.ingredients = aDecoder.decodeObject(forKey: SerializationKeys.ingredients) as? Ingredients
    self.srm = aDecoder.decodeObject(forKey: SerializationKeys.srm) as? Int
    self.method = aDecoder.decodeObject(forKey: SerializationKeys.method) as? Method
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.ebc = aDecoder.decodeObject(forKey: SerializationKeys.ebc) as? Int
    self.contributedBy = aDecoder.decodeObject(forKey: SerializationKeys.contributedBy) as? String
    self.ibu = aDecoder.decodeObject(forKey: SerializationKeys.ibu) as? Int
    self.targetOg = aDecoder.decodeObject(forKey: SerializationKeys.targetOg) as? Int
    self.targetFg = aDecoder.decodeObject(forKey: SerializationKeys.targetFg) as? Int
    self.abv = aDecoder.decodeObject(forKey: SerializationKeys.abv) as? Float
    self.volume = aDecoder.decodeObject(forKey: SerializationKeys.volume) as? Volume
    self.ph = aDecoder.decodeObject(forKey: SerializationKeys.ph) as? Float
    self.boilVolume = aDecoder.decodeObject(forKey: SerializationKeys.boilVolume) as? BoilVolume
    self.firstBrewed = aDecoder.decodeObject(forKey: SerializationKeys.firstBrewed) as? String
    self.brewersTips = aDecoder.decodeObject(forKey: SerializationKeys.brewersTips) as? String
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.foodPairing = aDecoder.decodeObject(forKey: SerializationKeys.foodPairing) as? [String]
    self.attenuationLevel = aDecoder.decodeObject(forKey: SerializationKeys.attenuationLevel) as? Int
    self.imageUrl = aDecoder.decodeObject(forKey: SerializationKeys.imageUrl) as? String
    self.tagline = aDecoder.decodeObject(forKey: SerializationKeys.tagline) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(ingredients, forKey: SerializationKeys.ingredients)
    aCoder.encode(srm, forKey: SerializationKeys.srm)
    aCoder.encode(method, forKey: SerializationKeys.method)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(ebc, forKey: SerializationKeys.ebc)
    aCoder.encode(contributedBy, forKey: SerializationKeys.contributedBy)
    aCoder.encode(ibu, forKey: SerializationKeys.ibu)
    aCoder.encode(targetOg, forKey: SerializationKeys.targetOg)
    aCoder.encode(targetFg, forKey: SerializationKeys.targetFg)
    aCoder.encode(abv, forKey: SerializationKeys.abv)
    aCoder.encode(volume, forKey: SerializationKeys.volume)
    aCoder.encode(ph, forKey: SerializationKeys.ph)
    aCoder.encode(boilVolume, forKey: SerializationKeys.boilVolume)
    aCoder.encode(firstBrewed, forKey: SerializationKeys.firstBrewed)
    aCoder.encode(brewersTips, forKey: SerializationKeys.brewersTips)
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(foodPairing, forKey: SerializationKeys.foodPairing)
    aCoder.encode(attenuationLevel, forKey: SerializationKeys.attenuationLevel)
    aCoder.encode(imageUrl, forKey: SerializationKeys.imageUrl)
    aCoder.encode(tagline, forKey: SerializationKeys.tagline)
  }

}

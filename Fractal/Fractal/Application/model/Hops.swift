//
//  Hops.swift
//
//  Created by Anderson Silva on 20/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Hops: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let attribute = "attribute"
    static let name = "name"
    static let amount = "amount"
    static let add = "add"
  }

  // MARK: Properties
  public var attribute: String?
  public var name: String?
  public var amount: Amount?
  public var add: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    attribute <- map[SerializationKeys.attribute]
    name <- map[SerializationKeys.name]
    amount <- map[SerializationKeys.amount]
    add <- map[SerializationKeys.add]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = attribute { dictionary[SerializationKeys.attribute] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = amount { dictionary[SerializationKeys.amount] = value.dictionaryRepresentation() }
    if let value = add { dictionary[SerializationKeys.add] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.attribute = aDecoder.decodeObject(forKey: SerializationKeys.attribute) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.amount = aDecoder.decodeObject(forKey: SerializationKeys.amount) as? Amount
    self.add = aDecoder.decodeObject(forKey: SerializationKeys.add) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(attribute, forKey: SerializationKeys.attribute)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(amount, forKey: SerializationKeys.amount)
    aCoder.encode(add, forKey: SerializationKeys.add)
  }

}

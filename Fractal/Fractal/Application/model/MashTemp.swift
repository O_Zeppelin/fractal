//
//  MashTemp.swift
//
//  Created by Anderson Silva on 20/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class MashTemp: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let temp = "temp"
    static let duration = "duration"
  }

  // MARK: Properties
  public var temp: Temp?
  public var duration: Int?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    temp <- map[SerializationKeys.temp]
    duration <- map[SerializationKeys.duration]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = temp { dictionary[SerializationKeys.temp] = value.dictionaryRepresentation() }
    if let value = duration { dictionary[SerializationKeys.duration] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.temp = aDecoder.decodeObject(forKey: SerializationKeys.temp) as? Temp
    self.duration = aDecoder.decodeObject(forKey: SerializationKeys.duration) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(temp, forKey: SerializationKeys.temp)
    aCoder.encode(duration, forKey: SerializationKeys.duration)
  }

}

//
//  Ingredients.swift
//
//  Created by Anderson Silva on 20/09/17
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class Ingredients: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let malt = "malt"
    static let hops = "hops"
    static let yeast = "yeast"
  }

  // MARK: Properties
  public var malt: [Malt]?
  public var hops: [Hops]?
  public var yeast: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    malt <- map[SerializationKeys.malt]
    hops <- map[SerializationKeys.hops]
    yeast <- map[SerializationKeys.yeast]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = malt { dictionary[SerializationKeys.malt] = value.map { $0.dictionaryRepresentation() } }
    if let value = hops { dictionary[SerializationKeys.hops] = value.map { $0.dictionaryRepresentation() } }
    if let value = yeast { dictionary[SerializationKeys.yeast] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.malt = aDecoder.decodeObject(forKey: SerializationKeys.malt) as? [Malt]
    self.hops = aDecoder.decodeObject(forKey: SerializationKeys.hops) as? [Hops]
    self.yeast = aDecoder.decodeObject(forKey: SerializationKeys.yeast) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(malt, forKey: SerializationKeys.malt)
    aCoder.encode(hops, forKey: SerializationKeys.hops)
    aCoder.encode(yeast, forKey: SerializationKeys.yeast)
  }

}
